#!/bin/bash

# install java
apt update
apt install -y openjdk-11-jre

# install redpen
wget https://github.com/redpen-cc/redpen/releases/download/redpen-1.10.3/redpen-1.10.3.tar.gz
tar xvf redpen-1.10.3.tar.gz
mkdir -p /user/local/redpen
mv redpen-distribution-1.10.3 /usr/local/redpen

export PATH="/usr/local/redpen/bin:${PATH}"

# clean
rm redpen-1.10.3.tar.gz

